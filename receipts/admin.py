from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account

# Register your models here.


@admin.register(ExpenseCategory)
class ExpensesCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "id",
    )


@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )
