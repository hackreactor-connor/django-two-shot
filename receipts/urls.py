from django.urls import path
from .views import (
    list_receipt,
    create_receipt,
    account_list,
    category_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("", list_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="accounts"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="categories"),
    path("categories/create/", create_category, name="create_category"),
]
